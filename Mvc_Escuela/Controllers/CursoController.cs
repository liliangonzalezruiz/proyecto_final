﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using escuela.Models;
     
namespace Mvc_Escuela.Controllers
{
    public class CursoController : Controller
    {
        // GET: Curso
        public ActionResult Index()
        {
            List<Curso> cursos = null;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("cursos");
                response.Wait();

                var resutl = response.Result;
                if (resutl.IsSuccessStatusCode)
                {
                    var readTask = resutl.Content.ReadAsAsync<List<Curso>>();
                    cursos = readTask.Result;
                }
                else

                {
                    cursos = new List<Curso>();
                    ModelState.AddModelError(string.Empty, "server error.plase contact administrador");

                }
            }

            return View(cursos);
        }

        // GET: Curso/Details/5
        public ActionResult Details(int id)
        {
            Curso cursos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("cursos/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Curso>();
                    readTask.Wait();
                    cursos = readTask.Result;
                }
                else

                {
                    cursos = new Curso();
                    ModelState.AddModelError(string.Empty, "server error.plase contact administrador");

                }
            }

            return View(cursos);
        }
            // GET: Curso/Create
            public ActionResult Create()
        {
            return View();
        }

        // POST: Curso/Create
        [HttpPost]
        public ActionResult Create(Curso cursos)
        {
            using (var client = new HttpClient())

            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                // http post
                var postTask = client.PostAsJsonAsync<Curso>("cursos", cursos);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)

                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, "server error.plase contact administrador");


                {
                    return View(cursos);
                }
            }
        }

            // GET: Curso/Edit/5
            public ActionResult Edit(int id)
            {
            Curso cursos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("cursos/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Curso>();
                    readTask.Wait();
                    cursos = readTask.Result;
                }
                else

                {
                    cursos = new Curso();
                    ModelState.AddModelError(string.Empty, "server error.plase contact administrador");

                }
            }

            return View(cursos);
        }

        // POST: Curso/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Curso Curso)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                // http post
                var putTask = client.PutAsJsonAsync<Curso>("Cursos/" + id.ToString(), Curso);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }


                return View(Curso);
            }


        }

        // GET: Curso/Delete/5
        public ActionResult Delete(int id)
        {
            Curso cursos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("cursos/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Curso>();
                    readTask.Wait();
                    cursos = readTask.Result;
                }
                else

                {
                    cursos = new Curso();
                    ModelState.AddModelError(string.Empty, "server error.plase contact administrador");

                }
            }

            return View(cursos);
        }
        // POST: Curso/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Curso Cursos)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Cursos/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(Cursos);
        }
    }
}
