﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using escuela.Models;
namespace Mvc_Escuela.Controllers
{
    public class InstructorController : Controller
    {
        // GET: Instructor
        public ActionResult Index()
        {
            List<Instructor> profesor = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Instructores");
                response.Wait();

                var resutl = response.Result;
                if (resutl.IsSuccessStatusCode)
                {
                    var readTask = resutl.Content.ReadAsAsync<List<Instructor>>();
                    profesor = readTask.Result;
                }
                else

                {
                    profesor = new List<Instructor>();
                    ModelState.AddModelError(string.Empty, "server error.plase contact administrador");

                }
            }

            return View(profesor);
        }

        // GET: Instructor/Details/5
        public ActionResult Details(int id)
        {
            Instructor profesor = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Instructor/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Instructor>();
                    readTask.Wait();
                    profesor = readTask.Result;
                }
                else

                {
                    profesor = new Instructor();
                    ModelState.AddModelError(string.Empty, "server error.plase contact administrador");

                }
            }
            return View(profesor);
        }

        // GET: Instructor/Create
        public ActionResult Create()
        {
            List<Instructor> profesor = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Instructores");
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Instructor>>();
                    readTask.Wait();
                    profesor = readTask.Result;
                }
                else
                {
                    profesor = new List<Instructor>();

                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
                ViewBag.Instructor = profesor;


                return View();
            }

        }
        // POST: Instructor/Create
        [HttpPost]
        public ActionResult Create(Instructor profesor)
        {
            using (var client = new HttpClient())

            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                // http post
                var postTask = client.PostAsJsonAsync<Instructor>("instructores", profesor);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)

                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, "server error.plase contact administrador");


                {
                    return View(profesor);
                }
            }
        }

            // GET: Instructor/Edit/5
            public ActionResult Edit(int id)
        {
            Instructor profesor = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Instructores/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Instructor>();
                    readTask.Wait();
                    profesor = readTask.Result;
                }
                else

                {
                    profesor = new Instructor();
                    ModelState.AddModelError(string.Empty, "server error.plase contact administrador");

                }
            }
            return View(profesor);
        }

        // POST: Instructor/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Instructor profesor)
        {
            
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44388/api/");
                    // http post
                    var putTask = client.PutAsJsonAsync<Instructor>("instructor/" + id.ToString(), profesor);
                    putTask.Wait();

                    var result = putTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }


                    return View(profesor);
                }
            

        }

        // GET: Instructor/Delete/5
        public ActionResult Delete(int id)
            {
            Instructor profesor = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Instructor/" + id.ToString());
                response.Wait();
                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Instructor>();
                    readTask.Wait();
                    profesor = readTask.Result;

                }
                else
                {
                    profesor = new Instructor();
                    ModelState.AddModelError(string.Empty, "server error.Please contact administrador.");
                }

                return View(profesor);
            }
        }



        // POST: Instructor/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Instructor profesor) 
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Instructor/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(profesor);
        }
    }
}
