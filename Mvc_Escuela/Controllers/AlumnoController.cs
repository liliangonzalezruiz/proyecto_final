﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using escuela.Models;
namespace Mvc_Escuela.Controllers
{
    public class AlumnoController : Controller
    {
        // GET: Alumno
        public ActionResult Index()
        {

            List<Curso> Cursos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Cursos");
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Curso>>();
                    readTask.Wait();
                    Cursos = readTask.Result;
                }
                else
                {
                    Cursos = new List<Curso>();

                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
            }

            ViewBag.Cursos = Cursos;

            List<Alumno> alm = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Alumnos");
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Alumno>>();
                    readTask.Wait();
                    alm = readTask.Result;
                }
                else
                {
                    alm = new List<Alumno>();

                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
            }
                return View(alm);
            
        }

        // GET: Alumno/Details/5
        public ActionResult Details(int id)
        {
            Alumno alm = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Alumnos/" + id.ToString());
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Alumno>();
                    readTask.Wait();
                    alm = readTask.Result;
                }
                else
                {
                    alm = new Alumno();
                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
            }

            return View(alm);
        }


        // GET: Alumno/Create
        public ActionResult Create()
        {
            List<Curso> cursos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
                var response = client.GetAsync("Cursos");
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Curso>>();
                    readTask.Wait();
                    cursos = readTask.Result;
                }
                else
                {
                    cursos = new List<Curso>();

                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
                ViewBag.Cursos = cursos;


                return View();
            }

        }
        // POST: Alumno/Create
        [HttpPost]
        public ActionResult Create(Alumno alm)
        { 
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44388/api/");
    //http post
               var postTask = client.PostAsJsonAsync<Alumno>("Alumnos", alm);
             postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
}
               ModelState.AddModelError(string.Empty, "server error. Please contact admistrator");
            }
            return View(alm);

        }

        // GET: Alumno/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Alumno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Alumno/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Alumno/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
